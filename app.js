const { Sequelize, QueryTypes } = require('sequelize');

require('dotenv').config();

const sequelize = new Sequelize({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.GB_HOST,
    dialect: process.env.DB_DIALECT
});

async function connect(){
    try{
        await sequelize.authenticate();
        console.log('Connection established...')

        // Countries
        const countries = await sequelize.query('SELECT * FROM country LIMIT 5', {
            type: QueryTypes.SELECT
        })
        const countryNames = countries.map(country => country.Name);
        console.log(countryNames);

        // Cities
        const cities = await sequelize.query('SELECT * FROM city LIMIT 5', {
            type: QueryTypes.SELECT
        })
        const cityNames = cities.map(city => city.Name);
        console.log(cityNames);

        // Languages
        const languages = await sequelize.query('SELECT * FROM countryLanguage LIMIT 5', {
            type: QueryTypes.SELECT
        })
        const languageNames = languages.map(countryLanguage => countryLanguage.Language);
        console.log(languageNames);
    }

    catch(e){
        console.error(e)
    }
}

connect();